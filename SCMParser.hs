module SCMParser where

import CFSM
import Automata

import Data.List as L
import Data.Map as M




-- Parser
import Text.ParserCombinators.Parsec
import qualified Text.ParserCombinators.Parsec.Token as T
import Text.ParserCombinators.Parsec.Language
import Text.ParserCombinators.Parsec.Error


-- DEBUG
import System.IO.Unsafe
import Debug.Trace

type PEdge = ((String, Direction, String), String)
type PTransition = (String, ((String, Direction, String), String))
type PMachine = (String, [PTransition])
type PSystem = [PMachine]

mainparser :: Parser PSystem
mainparser =  whiteSpace >> systemparser <* eof


parseSCMs :: String -> Either ParseError PSystem
parseSCMs inp =  parse mainparser "" inp



mkCFSM :: (Map String (String, String)) -> String -> PMachine -> Machine
mkCFSM chanmap id (init, trans) = Automaton { states = nub $ concat $ L.map (\(x,(y,z)) -> [x,z]) ntrans
                                            , sinit = init
                                            , transitions = ntrans
                                            }
  where ntrans = L.map (\(x,(y,z)) -> (x, (mklabel y, z))) trans
        mklabel (p,d,m) = if d == Send
                          then (id, snd $ chanmap M.! p, d, m)
                          else (fst $ chanmap M.! p, id, d, m)





getChannels :: PSystem -> Map String (String, String)
getChannels sys = M.fromList $
                  L.map (\x -> (x, mkChan x)) allchans
                  
  where machineChannels dir (i, xs) = L.map (\(src,((chan,d,m),trg)) -> chan) $
                                      L.filter (\(src,((chan,d,m),trg)) -> d==dir)  xs
        mtochans = L.map (\(n,m) -> (show n, (machineChannels Send m), (machineChannels Receive m))) $
                   snd $ mapAccumL (\x y -> (x+1,(x,y))) 0 sys
        allchans = L.nub $ concat $
                   L.map (\(id,s,r) -> s++r) mtochans
        senders chan = L.map (\(x,y,z) -> x) $
                       L.filter (\(id,s,r) -> chan `L.elem` s) mtochans
        receivers chan = L.map (\(x,y,z) -> x) $
                         L.filter (\(id,s,r) -> chan `L.elem` r) mtochans


        mkChan chan = case senders chan of
                       [] -> error $ "No sender for channel: "++(show chan)
                       [x] -> case receivers chan of
                               [] ->  error $ "No receiver for channel: "++(show chan)
                               [y] -> if x/=y then (x,y)
                                      else error $ "Sender=Receiver for channel: "++(show x)
                               (y:z:xs) -> error $ "Too many receivers for channel: "
                                           ++(show chan)++" ("++(show (x:z:xs))++")"
                       (x:z:xs) -> error $ "Too many senders for channel: "
                                   ++(show chan)++" ("++(show (x:z:xs))++")"


partner :: String -> String
partner s = case s of
             "0" -> "1"
             "1" -> "0"
             _ -> error $ "[SCMPartner] Unknown partner!"

mkSCMSystem :: PSystem -> System
mkSCMSystem sys = M.fromList $
                  L.map (\(x,y) -> (show x, mkCFSM chanmap (show x) y)) nsys
  where nsys = snd $ mapAccumL (\x y -> (x+1,(x,y))) 0 sys
        chanmap = getChannels sys


-- Lexer & Parser
lexer :: T.TokenParser ()
lexer = T.makeTokenParser languageDef


languageDef =
  emptyDef { T.commentStart    = "/*"
           , T.commentEnd      = "*/"
           , T.commentLine     = "//"
           , T.identStart      = alphaNum
           , T.identLetter     = alphaNum <|> (oneOf ['_'])
           , T.reservedNames   = ["automaton", "initial", "when", "true", "to","state","!","?",";"]
           , T.reservedOpNames = []
           , T.caseSensitive = True
           }

whiteSpace= T.whiteSpace lexer
lexeme    = T.lexeme lexer
symbol    = T.symbol lexer
parens    = T.parens lexer
poperator = T.operator lexer

participantid = T.identifier lexer  -- T.identifier lexer -- participant ID
pmessage = T.identifier lexer -- message string
pstate = T.identifier lexer

 
transitionParser :: Parser PEdge
transitionParser = do { symbol "to"
                      ; trg <- pstate
                      ; symbol ":"
                      ; symbol "when"
                      ; symbol "true"
                      ; symbol ","
                      ; partner <- participantid
                      ; dir <- (symbol "?" <|> symbol "!")
                      ; msg <- pmessage
                      ; symbol ";"
                      ; return $ ((partner, if dir=="!" then Send else Receive, msg), trg)
                      }

-- Forces machine to be CSA (may not be ideal!)
nonMixedTransitions :: [PEdge] -> [PEdge] 
nonMixedTransitions xs = case snds of
                          [] -> rcvs
                          _ ->  snds
                          -- case rcvs of
                          --      [] -> snds
                          --      _ -> if (length snds) <= (length rcvs)
                          --           then rcvs
                          --           else snds
  where flist dir =  L.filter (\((p,d,m), t) -> d==dir) xs
        snds = flist Send
        rcvs = flist Receive
        
footerParser :: Parser ()
footerParser = do { m <- many anyChar
                  ; return ()
                  }

stateParser :: Parser [PTransition]
stateParser = do { symbol "state"
                 ; src <- pstate
                 ; symbol ":"
                 ; edges <- many transitionParser
                 ; return $ L.map (\x -> (src,x)) $ edges
                 }

machineparser :: Parser PMachine
machineparser = do { symbol "automaton"
                   ; cmt <- participantid -- "Comment?"
                   ; symbol ":"
                   ; symbol "initial"
                   ; symbol ":"
                   ; init <- pstate
                   ; list <- many1 stateParser
                   ; return (init, concat list)
                   }
                
systemparser :: Parser PSystem
systemparser = do { list <- many1 machineparser
                  ; return list
                  }
               


-- isWFChoice list
