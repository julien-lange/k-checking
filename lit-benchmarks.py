#!/usr/bin/python3
import sys
import subprocess
import os
import os.path
import string
import time
# import numpy as np
import csv


# Number of iterations
maxiterations = 31

logfile = "lit-log-file-benchmarks.txt"

# OUTPUT FILE
prefname = "lit-benchmarks"

# TIMEOUT (in seconds)
# cmdtimeout = 360
# cmdtimeout = 1500 # 25 min
cmdtimeout = 1500 #


dotfiles = ["tests/Protocols/Fibonacci/fibo.txt"
            , "tests/Protocols/HTTP/http.txt"
            , "tests/Protocols/SAP-Negotiation/negotiate.txt"
            , "tests/Protocols/SH/sh.txt"
            , "tests/Protocols/SMTP/smtp.txt"
            , "tests/Protocols/TravelAgency/travelagency.txt"]

sessyfiles = ["tests/benchmarks/devsystem.txt"]
    
fsmfiles = [ "tests/benchmarks/AlternatingBit.txt"
               , "tests/benchmarks/Bargain.txt"
               , "tests/benchmarks/client-server-logger.txt"
               , "tests/benchmarks/CloudSystemV4.txt"
               , "tests/benchmarks/commit-protocol.txt"
               , "tests/benchmarks/devsystem-fsm.txt"
               ,  "tests/benchmarks/elevator-csa.txt"
             ,  "tests/benchmarks/elevator-extra.txt"
             ,  "tests/benchmarks/elevator-extra-variant.txt"
               ,  "tests/benchmarks/FilterCollaboration.txt"
             , "tests/benchmarks/gmc-runningexample"
               ,  "tests/benchmarks/fourplayergamer.txt"
               ,  "tests/benchmarks/HealthSystem.txt"
               ,  "tests/benchmarks/Logistic.txt"
             ,  "tests/benchmarks/SanitaryAgency.txt"
             , "tests/benchmarks/CloudSystemVFour.txt"
               ,  "tests/benchmarks/TPMContract.txt" ]


def cleanup(): 
    subprocess.call(["killall","KMC"]
                    , stdout=subprocess.PIPE
                    , stderr=subprocess.PIPE)


def runOverRange(name, filelist, filetype):
    with open(name,"a") as out:    
        write = csv.writer(out)
        with open(name+logfile, "ab") as log_file:
            for ex in filelist:
                timings = []
                nstates = ""
                ntrans = ""
                for it in range(1,maxiterations):
                    print("Running k-MC: ",ex)
                    startt = time.time() # time in seconds
                    #
                    # Use this to calculate size of RTS
                    # kmccmd = subprocess.Popen(["./KMC",ex,"1","2",filetype,"--debug","+RTS","-N8"], stdout=subprocess.PIPE)
                    #
                    kmccmd = subprocess.Popen(["./KMC",ex,"1",filetype,"--debug","+RTS","-N8"], stdout=subprocess.PIPE)
                    try:
                        kmccmd.wait(timeout=cmdtimeout)
                        endt = time.time()
                        txt = "Measured execution time: "+str(endt-startt)
                        print(txt)
                        for line in kmccmd.stdout:
                            sp = line.decode("utf-8").split("*")
                            if len(sp) > 4:
                                nstates = sp[1]
                                ntrans = sp[3]
                                log_file.write(line)
                        log_file.write((txt+"\n").encode())
                        timings.append(endt-startt)
                    except subprocess.TimeoutExpired:
                        kmccmd.kill()
                        kmccmd.wait()
                        print("KMC timedout")
                        return
                avg = sum(timings)/float(len(timings))
                write.writerow([ex,nstates,ntrans,avg]+timings)
    print("Saved in: ", name)                        


outputfile = "benchmarks-fromlit.csv"

runOverRange(outputfile, dotfiles, "--scm")
runOverRange(outputfile, sessyfiles, "--normal")
runOverRange(outputfile, fsmfiles, "--fsm")

# runOverRange(outputfile, ["tests/benchmarks/CloudSystemVFour.txt"], "--fsm")
