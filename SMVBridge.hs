module SMVBridge where


import Data.List as L
import Data.Map as M
import CFSM
import Automata

data KStructure = KStructure
                       { kstates :: [KState]
                       , kinit :: KState
                       , ktransitions :: [(KState,KState)]
                       } deriving (Show, Eq, Ord)


data KState = KState
              { configuration :: Configuration
              , availables :: Map Participant [Label]
              , label :: Maybe Label
              }
            deriving (Show, Eq, Ord)


getAvailables :: System -> Configuration -> Map Participant [Label]
getAvailables sys (smap, qmap) =
  M.fromList $ 
  L.map (\(k,v) -> (k, L.map fst $ successors (sys M.! k) v)) $ M.toList smap


makeKS :: System -> TS -> KStructure
makeKS sys ts = KStructure
                { kstates = nub $ concat $ L.map (\x -> [fst x, snd x]) ntrans
                , kinit = KState { configuration = sinit ts
                                 , availables = getAvailables sys (sinit ts)
                                 , label = Nothing
                                 }
                , ktransitions = ntrans
                }
  where ntrans = L.concat $ L.map (\(s,(l,t)) -> [(mkstate s, mktrans s l), (mktrans s l, mkstate t)]) $ transitions ts
        mkstate c = KState { configuration = c
                            , availables = getAvailables sys c
                            , label = Nothing
                            }
        mktrans c l = KState { configuration = c
                             , availables = getAvailables sys c
                             , label = Just l
                             }
                                       




kStructure2SMV :: System -> KStructure -> String
kStructure2SMV sys ks = "MODULE main\n"
                        ++"VAR\n"
                       
                        ++"id : 0.."++(show $ maximum $ M.elems envi)++";\n"
                        ++"localstates: array "++peerrange++" of 0.."++maxlstate++";\n"
                        ++"localdirection: array "++peerrange++ "of {SEND, RECEIVE, FINAL, MIXED};\n"
                        ++"queues: array "++peerrange++" of array "++peerrange++" of {EMPTY, "++(intercalate ", " alphabet)++"};\n"
                                               
                        ++"ASSIGN\n"
                        ++"init(id) := 0;\n"
                        ++"next(id) := case\n"++(intercalate ";\n" strans)++";\nTRUE : id;  \nesac;\n"
                        ++(intercalate "\n" (L.map localupdate (M.keys sys)))++"\n"
                        ++(intercalate "\n" (L.map localQupdate allqueues))++"\n"
                        ++(intercalate "\n" (L.map localDupdate (M.keys sys)))++"\n"
  where
    allqueues =  [(x,y) | x <- keys sys, y <- keys sys, x/=y]
    penvi = M.fromList $ snd $ mapAccumL (\x y -> (x+1, (y,x))) 0 (M.keys sys) :: Map Participant Int
    peerrange = "0.."++(show $ (length sys)-1)
    maxlstate = show $ maximum  $ L.map (\(k,v) -> length $ states v) $ M.toList sys
    strans = L.map (\(s,t) -> "id = "++(show $ envi M.! s)++" : "++(show $ envi M.! t)) $ ktransitions ks 
    envi = M.fromList $ snd $ mapAccumL (\x y -> (x+1, (y,x))) 0 (kstates ks)
    alphabet = nub $ concat $ L.map (\(k,v) -> malpha v) $ M.toList sys
    malpha m = L.map (\(s,((p,q,d,m),t)) -> m) $ transitions m


    getlocalstate m (KState (ss,qq) a l) = (ss M.! m)                                 
    mtrans m = L.map (\(s,t) -> "id = "++(show (envi M.! s))++" : "++(getlocalstate m t)) $ ktransitions ks
    localupdate m ="next(localstates["++(show $ penvi M.! m)++"]) := case\n"
                   ++(intercalate ";\n" (mtrans m))
                   ++";\nTRUE : localstates["++(show $ penvi M.! m)++"];  \nesac;"
    

    getqueue q (KState (ss,qq) a l) = case (qq M.! q) of
                                       [] -> "EMPTY"
                                       (x:xs) -> x
    
    qtrans q = L.map (\(s,t) -> "id = "++(show (envi M.! s))++" : "++(getqueue q t)) $ ktransitions ks
    localQupdate (p1,p2) ="next(queues["++(show $ penvi M.! p1)++"]["++(show $ penvi M.! p2)++"]) := case\n"
                    ++(intercalate ";\n" (qtrans (p1,p2)))
                    ++";\nTRUE : queues["++(show $ penvi M.! p1)++"]["++(show $ penvi M.! p2)++"];  \nesac;"


    getdirection m (KState (ss,qq) a l) = case successors (sys M.! m) (ss M.! m) of
                                           [] -> "FINAL"
                                           xs -> if L.and $ L.map (\x -> Send == (direction $ fst x)) xs
                                                 then "SEND"
                                                 else if  L.and $ L.map (\x -> Receive == (direction $ fst x)) xs
                                                      then "RECEIVE"
                                                      else "MIXED" 
    dtrans m = L.map (\(s,t) -> "id = "++(show (envi M.! s))++" : "++(getdirection m t)) $ ktransitions ks
    localDupdate m ="next(localdirection["++(show $ penvi M.! m)++"]) := case\n"
                    ++(intercalate ";\n" (dtrans m))
                    ++";\nTRUE : localdirection["++(show $ penvi M.! m)++"];  \nesac;"
                                                           
