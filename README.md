# THIS REPOSITORY IS NO LONGER MAINTAINED, USE THE [GITHUB REPO](https://github.com/julien-lange/kmc) INSTEAD!

# KMC: a tool for checking k-multiparty compatibility in communicating session automata

This is a tool that implements the theory introduced in "[_Verifying Asynchronous Interactions via
Communicating Session Automata_](https://arxiv.org/abs/1901.09606)" by Julien Lange and Nobuko Yoshida.

## Requirements:

You will need [GHC](https://www.haskell.org/platform/), [mCRL2](http://mcrl2.org), [Petrify](http://www.lsi.upc.edu/~jordicf/petrify/), and [GraphViz (dot)](https://www.graphviz.org/) installed on your machine (the last three are only required for the _experimental_ choreography synthesis part).


Depending on your setup, you might need to extra Haskell packages (GHC and Google will help you figure these out and you can install them with `cabal`). Petrify, mCRL2, and GraphViz are used for choreography construction only (`--synthesis` flag).

## Compile:

* Run: `ghc KMC -threaded` and `ghc GenCFSMs`, or

* If you have a working installation of `cabal`, run `cabal install` in the tool's directory.

## Usage

### Usage for k-MC checking:

* To find the least k s.t. k-MC holds for input file: `./KMC <path-to-automata> 1 <max-bound> +RTS -N<number-of-cpus>`

* To check for k-MC (of input file) inc. applicability checks: `./KMC <path-to-automata> <k>  +RTS -N<number-of-cpus>`

  This option will also check whether the system is CSA. If no bound is given, the tool assumes k=1.

### Usage for k-bounded choreography synthesis (_experimental_):

* To check for k-MC and synthesise a global graph: `./KMC <path-to-automata> <bound> --synthesis  +RTS -N<number-of-cpus>`

  Currently, the computation of the send projection is done rather naively, so the synthesis might take a while.

### Other usages:
  
* More information: `./KMC --help`

* To generate examples: `./GenCFSMs <n> <k> <m>` (run `ghc GenCFSMs.hs` first!)

* To run benchmark script for generated examples: `./parameterised-benchmarks.py` (run `ghc GenCFSMs.hs` first!)

* To run benchmark script for literature examples: `./lit-benchmarks.py`

* Examples are available in `./tests/` and `./tests/benchmarks/` contains examples from other papers.

## Input languages

### Syntactical session types (default)

KMC takes files that contain a list of "named" session types, e.g.,

    S: rec x . C?req; { C!ko;C?data;x
       	   	          , C!ok;C?data;L!log;end}

    L: S?log;end

    C: rec x . S!req;S!data; {S?ko;x, S?ok;end, S?err;end}


### CFSMs (use `--fsm` flag)

KMC takes the same input language as the [GMC tool](https://bitbucket.org/julien-lange/gmc-synthesis/), e.g.,

    -- This is machine #0
    .outputs
    .state graph
    q0 1 ! hello q1
    q0 1 ! bye q1
    .marking q0
    .end

    -- This is machine #1
    .outputs 
    .state graph
    q0 0 ? hello q1
    q0 0 ? bye q1
    .marking q0
    .end



## Usage via Docker

If you have Docker installed, you can also use `docker run jslange/kmc:nov18 <args>`, e.g.,

* `docker run jslange/kmc:nov18 tests/init1.txt 2 --debug`

* `docker run jslange/kmc:nov18 tests/benchmarks/client-server-logger.txt --fsm`

where these two files are internal to the container (all files on the git repository are included in the container).

If you would like to give your own file as a parameter use:

* `docker run -v <absolute-path-to-yourfile>:/tmp.txt jslange/kmc:nov18 /tmp.txt 2`
