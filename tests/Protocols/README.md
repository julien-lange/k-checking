Examples in this folder are from the paper Rumyana Neykova, Raymond
Hu, Nobuko Yoshida, Fahd Abdeljallal, "A session type provider:
compile-time API generation of distributed protocols with refinements
in F#" (CC 2018).