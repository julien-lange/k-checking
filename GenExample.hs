-- C: rec x . {D!a; {D?a;x, D?b;x}, D!b; {D?a;x, D?b;x}}
import System.Environment
import Data.List

msglist = ['a'..'z']
participants = ['A'..'Z']


mkType :: String -> String -> Int -> Int -> Int -> String
mkType dir p x 0 0 = "x"
mkType dir p x 0 oy = mkType "?" p x oy 0
mkType dir p x y oy = "{"++branchings++"}"
  where genOneBranch i = p++dir++([msglist!!i])++";"++(mkType dir p x (y-1) oy)      
        branchings = intercalate "," $ map genOneBranch [0..x]
        
        
mkRecursive :: String -> String 
mkRecursive s = "rec x."++s


mkSystem :: Int -> Int ->  [Char] -> String
mkSystem _ _ [] = ""
mkSystem x y (a:b:xs) = (mkcode a b)++"\n"++(mkcode b a)++"\n"++(mkSystem x y xs)
  where  mkcode a b = [a]++":"++(mkRecursive $ mkType "!" [b] x y y)

main :: IO ()
main = do args <- getArgs
          if ((length args) < 3)
            then do putStrLn "Usage: GenExample <breadth> <depth> <# of partition>"
                    return ()
            else do let x = let sx = read (args!!0) :: Int in if (sx > 0) && (sx < 26) 
                                                              then sx-1
                                                              else 0
                        y = let sy = read (args!!1) :: Int in if (sy > 0)
                                                              then sy
                                                              else 1
                        p = 2*(read (args!!2) :: Int)
                    putStrLn $ mkSystem x y $ take p participants
                    return ()