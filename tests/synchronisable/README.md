The examples in this directory are imported from "On the Completeness
of Verifying Message Passing Programs under Bounded Asynchrony" by
Ahmed Bouajjani, Constantin Enea, Kailiang Ji, Shaz Qadeer.

Some examples are replicated in the benchmarks folder.
