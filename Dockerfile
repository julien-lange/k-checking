FROM haskell:8

MAINTAINER Julien Lange <j.s.LASTNAME@kent.ac.uk>

WORKDIR /app

COPY . /app


RUN cabal update && \
    cabal install ansi-terminal text split parsec parallel cmdargs MissingH  && \
    cabal configure && \
    cabal install && \
    cabal build

RUN apt-get autoremove -y --purge && rm -rf /var/lib/apt/lists/*


ENTRYPOINT ["/root/.cabal/bin/kmc"]

CMD ["--help"]


# Pass <yourfile> as parameter:
# docker run -v <absolute-path-to-yourfile>:/tmpfile.txt kmc /tmpfile.txt